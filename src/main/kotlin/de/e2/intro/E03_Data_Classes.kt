package de.e2.intro.dataclasses

fun main(args: Array<String>) {
    class Circle1(val x: Int, val y: Int, val radius: Int) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Circle1

            if (x != other.x) return false
            if (y != other.y) return false
            if (radius != other.radius) return false

            return true
        }

        override fun hashCode(): Int {
            var result = x
            result = 31 * result + y
            result = 31 * result + radius
            return result
        }

        override fun toString(): String {
            return "Circle1(x=$x, y=$y, radius=$radius)"
        }
    }

    val c1 = Circle1(1, 1, 10)
    val c2 = Circle1(c1.x, c1.y, 100)

    //== vergleicht mit !equals / === vergleicht Identität
    if (c1 == c2) {
        println("$c1 == $c2")
    } else {
        println("$c1 <> $c2")
    }

    data class Circle2(val x: Int, val y: Int, val radius: Int)

    val c3 = Circle2(1, 1, 10)
    val c4 = c3.copy(radius = 100)

    //== vergleicht mit equals() / === vergleicht Identität
    if (c3 == c4) {
        println("$c3 == $c4")
    } else {
        println("$c3 <> $c4")
    }

    println(c3) // Circle1(x=1, y=1, radius=100)

    val circleList = listOf(c3, c4)

    //Destructuring von Data Classes ist möglich
    for ((x, y) in circleList) {
        println("Circle with coordinates ($x,$y)")
    }

    fun divideWithRemainder(x: Int, y: Int) = Pair(x / y, x % y)

    //Funktionen mit mehreren Ergebnissen geht mit Destructuring ganz einfach
    val (result, remainder) = divideWithRemainder(10,3)

    println("$result $remainder")
}