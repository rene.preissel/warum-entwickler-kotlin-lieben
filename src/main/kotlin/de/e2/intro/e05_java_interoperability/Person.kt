package de.e2.intro.e05_java_interoperability

import java.time.LocalDate

class Person @JvmOverloads constructor(
    val name: String, val vorname: String,
    val geburtsname: String? = null,
    val geburtsdatum: LocalDate? = null
)