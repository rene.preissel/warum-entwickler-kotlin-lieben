package de.e2.intro.e05_java_interoperability;

public class UsePerson {
    public static void main(String[] args) {
        Person person = new Person("Preissel", "Rene");
        System.out.println(person.getName());

        int years = DateExtensionKt.fullYearsUntilToday(person.getGeburtsdatum());
    }
}
