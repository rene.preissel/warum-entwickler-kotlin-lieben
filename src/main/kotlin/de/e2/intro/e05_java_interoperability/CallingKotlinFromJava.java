package de.e2.intro.e05_java_interoperability;

public class CallingKotlinFromJava {
    public static void main(String[] args) {
        KotlinClass kotlinClass = new KotlinClass("foo");
        kotlinClass.printName1();
        kotlinClass.druckeName();
    }
}
