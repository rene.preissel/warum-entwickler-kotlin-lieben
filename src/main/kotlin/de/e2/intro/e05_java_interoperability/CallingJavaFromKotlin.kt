package de.e2.intro.e05_java_interoperability

import java.io.Serializable
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future


fun main(args: Array<String>) {
    class Data : Serializable

    val date = Date(0)
    val localDate =
        LocalDate.ofInstant(date.toInstant(), ZoneId.systemDefault())
    val day: Int = localDate.dayOfMonth
    val month: Int = localDate.month.value

    val arrayList: List<String> = ArrayList()
    val mutableArrayList: MutableList<String> = ArrayList()

    val executor = Executors.newCachedThreadPool()
    val result: Future<Int> = executor.submit(Callable {
        1 + 2
    })
}