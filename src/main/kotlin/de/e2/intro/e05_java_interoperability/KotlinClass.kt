package de.e2.intro.e05_java_interoperability

data class KotlinClass(val name: String) {
    fun printName1() {
        println(name)
    }

    //Namen können für Java anders benannt werden
    @JvmName(name = "druckeName")
    fun printName2() {
        println(name)
    }
}