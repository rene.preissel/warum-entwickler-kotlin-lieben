package de.e2.intro.e05_java_interoperability

import java.time.LocalDate
import java.time.Period

fun LocalDate.fullYearsUntilToday() =
    Period.between(this, LocalDate.now()).years
