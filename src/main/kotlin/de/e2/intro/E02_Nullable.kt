package de.e2.intro.nullable


class Adresse(val stadt: String)
//Typen ohne ? sind niemals Null, ? erzeugt Nullable Typen
class Person(
    val name: String, val geburtstname: String?,
    val adresse: Adresse?
)

fun main(args: Array<String>) {
    val rene = Person("Rene Preissel", null, Adresse("Hamburg"))

    val anke = Person("Anke Preissel", "Baumann", Adresse("Hamburg"))

    val weihnachtsmann1 = Person("Santa Claus", null, null)

    fun findePerson(name: String): Person? {
        return null
    }

    //Funktion erwartet niemals Null
    fun druckeAdresse(adresse: Adresse) {
        println(adresse.stadt)
    }

    val weihnachtsmann: Person? = findePerson("Santa Claus")
//    val adresse1: Adresse? = weihnachtsmann.adresse // Compile Error
    val adresse2: Adresse? = weihnachtsmann?.adresse // Compile Error
//    druckeAdresse(adresse2) // Compile Error
    if (adresse2 != null) {
        druckeAdresse(adresse2)
    }

    //druckeAdresse(rene.adresse)  //compile error

    if (rene.adresse != null) {
        //Smart Cast zu Nicht-Nullable Typ
        druckeAdresse(rene.adresse)
    }

    //?. ist Null-Sicher für Navigation
    val stadt1: String? = anke.adresse?.stadt

    //?: wird benutzt wenn der Wert davor null ist -> Elvis Operator
    val stadt2: String = weihnachtsmann?.adresse?.stadt ?: "unbekannt"

    //?: geht auch mit throw oder return
    val stadt3: String = rene.adresse?.stadt ?: throw IllegalStateException("Renes Adresse kann nicht null sein")

//    var nullableString: String? = "abc"
//    nullableString = null
//
//    var noneNullableString: String = "abc"
//    noneNullableString = null   //Compile Error
}


