package de.e2.intro.e08_coroutines.blocking


import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis


typealias URL = String
typealias Image = String

fun requestImages(query: String, count: Int): List<Image> =
    requestImageUrls(query, count).map { url ->
        requestImageData(url)
    }

val executor = Executors.newCachedThreadPool { r ->
    thread(start = false, isDaemon = true) { r.run() }
}

fun requestImagesParallel(query: String, count: Int): List<Image> =
    requestImageUrls(query, count).map { url ->
        executor.submit(Callable { requestImageData(url) })
    }.map { future ->
        future.get()
    }


fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val images = requestImages("car", 20)
        println(images)
    }
    println("Time: $time")

    val timeParallel = measureTimeMillis {
        val images = requestImagesParallel("car", 20)
        println(images)
    }
    println("TimeParallel: $timeParallel")
}


//Nachfolgende Funktionen simulieren von synchronen Aufrufen

fun requestImageUrls(query: String, count: Int): List<URL> =
    (1..count).map { "$query $it" }


fun requestImageData(url: URL): Image {
    Thread.sleep(Random().nextInt(100).toLong()) //Nur für den Test
    return "$url: data"
}
