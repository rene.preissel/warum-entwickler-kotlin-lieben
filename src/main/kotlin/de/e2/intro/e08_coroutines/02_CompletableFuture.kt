package de.e2.intro.e08_coroutines.futures

import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis


typealias URL = String
typealias Image = String


fun requestImages(query: String, count: Int): CompletableFuture<List<Image>> =
    requestImageUrls(query, count)
        .thenCompose { urls ->
            val startFuture = CompletableFuture.completedFuture<List<Image>>(listOf())
            urls.fold(startFuture) { lastFuture, url ->
                lastFuture.thenCompose { images ->
                    requestImageData(url).thenApply { image ->
                        images + image
                    }
                }
            }
        }

fun requestImagesParallel(query: String, count: Int): CompletableFuture<List<Image>> =
    requestImageUrls(query, count)
        .thenCompose { urls ->
            val result = CompletableFuture<List<Image>>()

            val imageFutureList = urls.map { url ->
                requestImageData(url)
            }

            CompletableFuture.allOf(*imageFutureList.toTypedArray()).whenComplete { _, _ ->
                result.complete(imageFutureList.map { imageFuture ->
                    imageFuture.get()
                })
            }

            result
        }


fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val images = requestImages("car", 20).join()
        println(images)
    }
    println("Time: $time")

    val timeParallel = measureTimeMillis {
        val images = requestImagesParallel("car", 20).join()
        println(images)
    }
    println("TimeParallel: $timeParallel")
}

//Nachfolgende Funktionen simulieren nur asynchrone Aufrufe

fun requestImageUrls(query: String, count: Int): CompletableFuture<List<URL>> =
    CompletableFuture.completedFuture((1..count).map { "$query $it" })

val scheduler = Executors.newSingleThreadScheduledExecutor()

fun requestImageData(url: URL): CompletableFuture<Image> {
    val result = CompletableFuture<URL>()
    scheduler.schedule(
        { -> result.complete("$url: data") },
        Random().nextInt(100).toLong(),
        TimeUnit.MILLISECONDS
    )
    return result
}
