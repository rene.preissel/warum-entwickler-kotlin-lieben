package de.e2.intro.e08_coroutines.coroutines

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.selects.select
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.system.measureTimeMillis


typealias URL = String
typealias Image = String


suspend fun requestImages(query: String, count: Int): List<Image> =
    requestImageUrls(query, count).map { url ->
        requestImageData(url)
    }


suspend fun requestImagesParallel(query: String, count: Int): List<Image> = coroutineScope {
    requestImageUrls(query, count).map { url ->
        async { requestImageData(url) }
    }.map { deferredImage ->
        deferredImage.await()
    }
}

suspend fun requestImagesParallelInPerformanceOrder(query: String, count: Int): List<Image> = coroutineScope {
    val deferredList = requestImageUrls(query, count).map {
        async { requestImageData(it) }
    }.toMutableList()
    val resultList = mutableListOf<Image>()
    while (deferredList.isNotEmpty()) {
        val deferredFinished: Deferred<Image> = select {
            deferredList.forEach { deferred ->
                deferred.onAwait { _ -> deferred }
            }
        }

        deferredList.remove(deferredFinished)
        resultList.add(deferredFinished.getCompleted())
    }
    resultList
}


suspend fun main() {
    val time = measureTimeMillis {
        val images = requestImages("car", 20)
        println(images)
    }
    println("Time: $time")

    val timeParallel = measureTimeMillis {
        val images = requestImagesParallel("car", 20)
        println(images)
    }
    println("TimeParallel: $timeParallel")

    val timeParallelPerformanceOrder = measureTimeMillis {
        val images = requestImagesParallelInPerformanceOrder("car", 20)
        println(images)
    }
    println("TimeParallelPerformanceOrder: $timeParallelPerformanceOrder")

}


//Nachfolgende Funktionen simulieren nur asynchrone Aufrufe

suspend fun requestImageUrls(query: String, count: Int): List<URL> =
    suspendCoroutine { continuation ->
        continuation.resume((1..count).map { "$query $it" })
    }

suspend fun requestImageData(url: URL): Image {
    delay(Random().nextInt(100).toLong()) //Nur für den Test
    return suspendCoroutine { continuation ->
        continuation.resume("$url: data")
    }
}
