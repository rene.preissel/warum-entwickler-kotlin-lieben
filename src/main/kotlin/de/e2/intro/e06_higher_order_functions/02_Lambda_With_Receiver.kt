package de.e2.intro.e06_higher_order_functions

import java.time.LocalDate
import java.time.Period

fun main(args: Array<String>) {

    //Anderes Beispiel für Lambdas
    class LoggingSupport(val prefix: String) {
        fun log(msg: String) {
            println("$prefix - $msg")
        }
    }

    fun withLogging1(prefix: String, block: (LoggingSupport) -> Unit) {
        val logging = LoggingSupport(prefix)
        block(logging)
    }

    //Parameter für Lambdas können eigenen Namen bekommen
    withLogging1("test 1") { logger ->
        logger.log("Log 1")
        logger.log("Log 2")
    }

    //Lambdas with Receiver sind Lambdas bei denen der This-Zeiger innerhalb des Blockes redefiniert wird
    fun withLogging2(prefix: String, block: LoggingSupport.() -> Unit) {
        val context = LoggingSupport(prefix)
        context.block()
    }

    withLogging2("test 2") {
        //Inneres This ist hier LoggingSupport
        log("Log 1")
        log("Log 2")
    }

    val today = LocalDate.now()
    with(today) {
        println("$dayOfMonth.${month.value}.$year")
    }

    //Extension Function
    fun LocalDate.age(): Int = Period.between(this, LocalDate.now()).years

    val birthDate = LocalDate.of(2000, 8, 1)

    //Combination of Lambda 'takeIf', NullSafety '?.' und LambdaWithReceiver 'run'
    birthDate.age().takeIf { it >= 18 }?.run {
        println("For adults only. I'm $this years old")
    }
}