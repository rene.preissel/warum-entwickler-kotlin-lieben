package de.e2.intro.e06_higher_order_functions

//Inline Funktionen werden an die Aufrufstelle kopiert
inline fun withTracing(
    prefix: String,
    traceEntry: Boolean = true,
    traceExit: Boolean = true,
    block: () -> Unit
) {
    //Die Ifs werden optimiert, wenn die Boolsche-Variable zur Compile-Zeit bekannt ist.
    if (traceEntry) {
        println("$prefix - Entry")
    }
    if (traceExit) {
        try {
            block()
        } finally {
            println("$prefix - Exit")
        }
    } else {
        block()
    }
}


fun main(args: Array<String>) {
    withTracing("test-with-exit") {
        println("MyFunction")
    }

    withTracing("test-without-exit", traceExit = false) {
        println("MyFunction")
    }

    //entspricht
    //println("test-without-exit - Entry")
    //println("MyFunction")
}