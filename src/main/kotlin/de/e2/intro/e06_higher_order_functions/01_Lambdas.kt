package de.e2.intro.e06_higher_order_functions

fun main(args: Array<String>) {

    val numbers: List<Int> = (1..10).toList()

    //Lambdas werden in {} geschrieben
    val evenNumbers1 = numbers.filter({ n -> n % 2 == 0 })

    //Lambdas als letzter Parameter können auch ohne runde Klamemrn aufgerufen werden
    val squaredEvenNumbers =
        numbers.filter { it % 2 == 0 }.map { it * it }
    squaredEvenNumbers.forEachIndexed { idx, number ->
        println("$idx = $number")
    }

    //Kotlin uterstützt Funktionstypen direkt und nicht nur per SAM (Int) -> Unit
    //Unit steht für kein Ergebnis
    fun repeat(times: Int, block: (Int) -> Unit) {
        for (i in 1..times) {
            block(i)
        }
    }

    //Aufrufe sehen dann aus wie eigene Kontrollstrukturen
    repeat(5) {
        //It ist der Default-Name für den Lambda-Parameter
        println(it) // 1, 2, 3, 4, 5
    }

    repeat(5) { idx ->
        println(idx) // 1, 2, 3, 4, 5
    }
}