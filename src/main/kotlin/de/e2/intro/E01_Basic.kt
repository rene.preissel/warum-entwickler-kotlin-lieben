package de.e2.intro.basic

fun main(args: Array<String>) {

    //Variables
    val a: Int = 10;
    val b = 10 // Ohne Typ und ohne Semikolon

    var c = 10 // val sind final
    c = 20  // und var sind änderbar

    //Funktionen - Global ist möglich
    fun add1(a: Int, b: Int): Int {
        return a + b
    }

    //Wenn die Funktion nur aus einer Expression besteht, dann geht auch =
    fun add2(a: Int, b: Int): Int = a + b

    //Return-Typ kann weggelassen werden
    fun add3(a: Int, b: Int) = a + b


    //Klassen
    class Person1 {
        private val vorname: String
        private val nachname: String

        constructor(vorname: String, nachname: String) {
            this.vorname = vorname;
            this.nachname = nachname
        }

        fun getVorname() = vorname
        fun getNachname() = nachname
    }

    class Person2 {
        //Bekommt automatisch einen Public-Getter, vars auch einen Setter
        val vorname: String
        val nachname: String

        constructor(vorname: String, nachname: String) {
            this.vorname = vorname;
            this.nachname = nachname
        }
    }

    //Primärer Konstruktor direkt nach dem Klassennamen
    class Person3(vorname: String, nachname: String) {

        //Parameter können bei der Initialisierung verwendet werden
        val vorname: String = vorname
        val nachname: String = nachname
    }

    //Im primären Konstruktor können direkt die Properties definiert werden
    class Person4(val vorname: String, val nachname: String)


    class Person5(val vorname: String, val nachname: String) {
        //Properties mit eigenen Getter
        val fullName
            get() = vorname + " " + nachname
    }


    class Person6(val vorname: String, val nachname: String) {
        val fullName
            get() = "$vorname $nachname" //Template-Strings erlauben das Benutzen von Variablen und Funktionsaufrufen
    }

    //Erzeugen von Objekten ohne new
    val personA = Person6("Rene", "Preissel")

    //Parameter können mit Namen aufgerufen werden, bessere Lesbarkeit, Reihenfolge ist dann egal
    val personB = Person6(nachname = "Preissel", vorname = "Rene")

    var myarrayList = ArrayList<String>()

    myarrayList.add("+23")
    myarrayList.add("-25")
    myarrayList.add("+125")
    myarrayList.add("+455")
    myarrayList.add("")
    myarrayList.add("*230")
    myarrayList.add("-293")
    myarrayList.add("/6")
    myarrayList.add("")
    myarrayList.add("+293")
    myarrayList.add("")
    myarrayList.add("+21")

    val splitPoints =
        myarrayList.mapIndexedNotNull() { idx, str -> if (str.isEmpty()) idx else null }


    val splittedList =
        (listOf(-1)  + splitPoints + myarrayList.size)
            .windowed(2, 1)
            .map { (start, end) -> myarrayList.subList(start + 1, end) }

    println(splittedList)
}