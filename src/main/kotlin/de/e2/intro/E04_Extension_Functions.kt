package de.e2.intro.extensions

import java.net.URL

fun main(args: Array<String>) {
    fun toURL(s: String) = URL(s)

    val url1 = toURL("http://localhost")

    //Erweiterung der String-Klasse um Funktion toURL()
    fun String.toURL() = URL(this)

    //Aufruf erfolgt wie bei normaler Methode
    val url2 = "http://localhost".toURL()

    class Person(val name: String, val email: String)

    //Macht auch Sinn bei eigenen Klassen, wenn diese in verschiedenen Modulen liegen
    data class PersonDTO(val name: String)
    fun Person.toDTO(): PersonDTO = PersonDTO(name)

    val rene = Person("Rene", "rp@etosquare.de")
    val reneDTO = rene.toDTO()

    println(reneDTO)

    val name: String? = "rene"
    if(!name.isNullOrBlank()) {
        val len = name.length //Smart Cast
    }

    val numbers: List<Int> = listOf()
    val sum = numbers.sum()

}
