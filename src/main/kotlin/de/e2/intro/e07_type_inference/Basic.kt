package de.e2.intro.e07_type_inference

fun main(args: Array<String>) {
    //Keine Angabe von expliziten Typen, führt trotzdem zu List<Int>

    val name = "Rene" //String


    fun add(a: Int, b: Int) = a + b //returns Int

    val numbers = listOf(1, 2, 3)

    val intArray = numbers.toTypedArray()
}

