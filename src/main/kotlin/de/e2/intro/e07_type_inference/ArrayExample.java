package de.e2.intro.e07_type_inference;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ArrayExample {
    public static void main(String[] args) throws IOException {
        List<Integer> numbers = Arrays.asList(1, 2, 3);

        Integer[] intArray = numbers.toArray(new Integer[0]);

        String json = "{" +
                "\"vorname\": \"Rene\",\n" +
                "\"nachname\": \"Preissel\"" +
                "}";

        Person person = new ObjectMapper().readValue(json, Person.class);
    }
}
