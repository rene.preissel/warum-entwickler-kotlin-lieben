package de.e2.intro.e07_type_inference

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper


data class Person(val nachname: String, val vorname: String)

//Reified ermöglich den Zugriff auf die Klasses des generischen Parameters in der Funktion
inline fun <reified T> ObjectMapper.readValue(json: String) : T = readValue(json, T::class.java)

fun main(args: Array<String>) {

    //Raw Strings über mehrere Zeilen
    val json = """
       {
            "vorname": "Rene",
            "nachname": "Preissel"
        }
    """

    val objectMapper = jacksonObjectMapper()

    //Klasse muss explizit übergeben werden, wegen Type-Erasure in der JVM
    val person1 = objectMapper.readValue(json, Person::class.java)
    println(person1)

    //Typ erkannt anhand des Typs der Variablen
    val person2: Person = objectMapper.readValue(json)
    println(person2)

    //Typ explizit als generischen Parameter übergeben
    val person3 = objectMapper.readValue<Person>(json)
    println(person3)

    fun doSomethingWithPerson(person: Person) {
        println(person)
    }

    //Typ erkannt anahand des Methoden-Parameters
    doSomethingWithPerson(objectMapper.readValue(json))
}

