import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.dsl.Coroutines

val kotlinCoroutineVersion: String by extra { "1.0.1" }

plugins {
    val kotlinVersion = "1.3.10"
    id("org.jetbrains.kotlin.jvm") version kotlinVersion
}

group = "de.e2"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
}


dependencies {
    compile("org.jetbrains.kotlin:kotlin-runtime")
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compile("org.jetbrains.kotlin:kotlin-reflect")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.6")

    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutineVersion")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:$kotlinCoroutineVersion")
}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict")
}
